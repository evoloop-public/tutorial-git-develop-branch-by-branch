# Tutorial Git - Develop branch-by-branch

> Author: Oscar Locatelli | Last Edit: 2021-03-12 Oscar Locatelli

## Demo 1

#### Gitlab, Clone, Edit, Push

* `git checkout step1` [step1 - Basic Local Workflow](https://gitlab.com/evoloopsrl/tutorial-git-develop-branch-by-branch/-/tree/step1)
  * git init
  * git status
  * git log
  * git add
  * git commit
  * git checkout
  * git stash
* `git checkout step2` [step2 - Basic Remote Workflow](https://gitlab.com/evoloopsrl/tutorial-git-develop-branch-by-branch/-/tree/step2)
  * git clone
  * git remote
  * git push
  * git pull
  * git merge
  * History Graph
  * Merge Tools
  * Gitlab

## Demo 2

#### Feature, Hotfix, Merge

* `git checkout step3` [step3 - Feature, Hotfix](https://gitlab.com/evoloopsrl/tutorial-git-develop-branch-by-branch/-/tree/step3)

## Extra

* Porting to git a svn project [step4 - Extra](https://gitlab.com/evoloopsrl/tutorial-git-develop-branch-by-branch/-/tree/step4)